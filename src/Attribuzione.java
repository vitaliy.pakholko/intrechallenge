public class Attribuzione
{
    private String user;
    private String idArea;

    public Attribuzione(String user, String idArea)
    {
        this.user = user;
        this.idArea = idArea;
    }

    public String getUser()
    {
        return user;
    }

    public String getIdArea()
    {
        return idArea;
    }

    public boolean operatorMatch(Operatore operatore)
    {
        return this.user.equals(operatore.getUsername());
    }

    public String toString()
    {
        return "L'attribuzione consiste nella combo di "+idArea+" e di "+user+"\n";
    }
}
