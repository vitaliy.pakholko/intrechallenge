import java.util.List;

public class Main
{
    public static void main(String[] args) throws Exception {
        AreaTree tree = new AreaTree(System.getProperty("user.dir")+"\\src\\Aree.txt");

        AreaManager manager1 = new AreaManager(tree);
        Operatore enel = new Operatore("Enel");
        manager1.addOperator(enel);
        System.out.println(manager1.getAttribuzioni());
        manager1.addOperator(enel);
        Operatore eni = new Operatore("Eni");
        manager1.removeOperator(eni);
        manager1.removeOperator(enel);

        manager1.addAttribuzione("CFT1",eni);
        manager1.addAttribuzione("CFT2",eni);
        System.out.println(manager1.getAttribuzioni());
    }
}
