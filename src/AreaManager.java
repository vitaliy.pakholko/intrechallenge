import java.util.*;
public class AreaManager
{
    private List<Operatore> operatori;
    private List<Attribuzione> attribuzioni;
    private AreaTree areaDaGestire;

    public List<Operatore> getOperatori()
    {
        return operatori;
    }

    public List<Attribuzione> getAttribuzioni()
    {
        return attribuzioni;
    }

        public AreaManager(AreaTree areaDaGestire) {

        this.areaDaGestire = areaDaGestire;
        operatori = new LinkedList<>();
        attribuzioni = new LinkedList<>();
    }



    public int restituisciOperatoreZona(Area area)
    {
        int i = 0;
        while(i<attribuzioni.size())
        {
            if(attribuzioni.get(i).getIdArea().equals(area.getCode()))
            {
                return i;
            }
            i++;
        }
        return -1;
    }

    public int attribuzioneContieneOperatore(Operatore operatore)
    {
        int i=0;
        while(i<attribuzioni.size())
        {
            if(attribuzioni.get(i).getUser().equals(operatore.getUsername()))
                return i;
            i++;
        }
        return -1;
    }

    public int operatoriContieneOperatore(Operatore operatore)
    {
        int i=0;
        while(i<operatori.size())
        {
            if(this.operatori.get(i).getUsername().equals(operatore.getUsername()))
                return i;
            i++;
        }
        return -1;
    }

    public boolean removeOperator(Operatore operatoreDaRimuovere)
    {
        int indiceOperatoreOperatori = operatoriContieneOperatore(operatoreDaRimuovere);
        if(indiceOperatoreOperatori==-1)
        {
            System.out.println("Operatore "+operatoreDaRimuovere.getUsername()+" non esiste tra gli operatori\n");
            return false;
        }
        else
        {
            int indiceOperatoreAttribuzioni = attribuzioneContieneOperatore(operatoreDaRimuovere);
            if(indiceOperatoreAttribuzioni==-1)
            {
                operatori.remove(indiceOperatoreAttribuzioni);
                System.out.println("Rimosso: "+operatoreDaRimuovere.getUsername()+"\n");
            }
            else
            {
                System.out.println("Operatore nelle attribuzioni\n");
            }
            return true;
        }

    }

    public void addOperator(Operatore operatoreDaAggiungere) throws Exception {
        if (operatori.isEmpty() && !attribuzioni.isEmpty())
        {
            System.out.println("Fatal Error state not allowed: If operatori is empty, attribuzioni should be empty too");
            throw new Exception();
        }
        if (operatori.isEmpty())
        {
            List<AreaNode> areePresenti = areaDaGestire.getAreas(areaDaGestire.getRoot());
            List<AreaNode> zone = areaDaGestire.getAreasOfCertainLevel(1);

            while(!zone.isEmpty())
            {
                AreaNode zona = zone.get(0);
                zone.remove(0);
                attribuzioni.add(new Attribuzione(operatoreDaAggiungere.getUsername(), zona.getArea().getCode()));
            }


        }
        int indiceOperatoreOperatori = operatoriContieneOperatore(operatoreDaAggiungere);
        if(operatoriContieneOperatore(operatoreDaAggiungere)==-1)
        {
            System.out.println("Operatore aggiunto "+operatoreDaAggiungere.getUsername()+"\n");
            operatori.add(operatoreDaAggiungere);
        }
        else
            System.out.println("Operatore "+operatoreDaAggiungere.getUsername()+" gia' presente");
    }

    public int indiceAreaAttribuzioni(Area areaDaCercare)
    {
        int i=0;
        while(i<attribuzioni.size())
        {
            if(attribuzioni.get(i).getIdArea().equals(areaDaCercare.getCode()))
                return i;
            i++;
        }
        return -1;
    }


    public void addAttribuzione(String areaID, Operatore operatore)
    {
        AreaNode areaTrovata = areaDaGestire.searchNodeByAreaID(areaDaGestire.getRoot(), areaID);
        if(areaTrovata!=null)
        {
            areaTrovata = areaTrovata.getParent();
            if(indiceAreaAttribuzioni(areaTrovata.getLeft().getArea())==-1 && (indiceAreaAttribuzioni(areaTrovata.getRight().getArea())==-1))
            {
                this.attribuzioni.add(new Attribuzione(operatore.getUsername(),areaID));
                System.out.println("Attribuzione aggiunta :"+areaID+" "+operatore.getUsername()+"\n");
            }

            else
                System.out.println("Area inoccupabile perche' il padre non controlla entrambe le sottozone");
        }
        else
            System.out.println("Area non trovata");

    }
}
