import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

public class AreaTree
{
    AreaNode root = null;

    public AreaNode getRoot() {
        return root;
    }


    AreaNode lastNode = null;

    public AreaTree(String path)
    {
        if (! readAreasFromFile(path).isEmpty())
        {
            this.appendAreas(readAreasFromFile(path));
        }


    }

    private void appendAreas(List<AreaNode> nodes)
    {
        int depth = 0;
        if (nodes.get(0).getArea().getCode().equals("MA"))
        {
            root = nodes.get(0);
            root.setDepth(depth);
            depth++;
            nodes.remove(0);
            lastNode = root;
            System.out.println(root+"\n");
        }
        else
            System.out.println("First element is not MA as it should be");



        while (!nodes.isEmpty())
        {
            AreaNode temp = findFirstEmpty(findFillable(root, depth));

            if(temp.getLeft()==null)
            {
                temp.setLeft(nodes.get(0));
                temp.getLeft().setParent(temp);
                temp.getLeft().setDepth(temp.getDepth()+1);
                nodes.remove(0);
                System.out.println(temp.getLeft());
                System.out.println("Ed il mio padre e'\n"+temp+"\n");
            }
            else
            {

                if(temp.getRight()==null)
                {
                    temp.setRight(nodes.get(0));
                    temp.getRight().setParent(temp);
                    temp.getRight().setDepth(temp.getDepth()+1);
                    nodes.remove(0);
                    System.out.println(temp.getRight());
                    System.out.println("Ed il mio padre e'\n"+temp+"\n");

                }
            }
        }

    }

    private List<AreaNode> findFillable(AreaNode node, int depth)
    {
        List<AreaNode> fillableNodes = new LinkedList<AreaNode>();

        if(((node.isLeftNull())||(node.isRightNull()))  && ((node.getDepth()==depth-1)))
        {
            fillableNodes.add(node);
        }
        else
        {
            if (!node.isLeftNull())
            {
                fillableNodes.addAll(findFillable(node.getLeft(), depth + 1));
            }
            if (!node.isRightNull())
            {
                fillableNodes.addAll(findFillable(node.getRight(), depth + 1));
            }
        }

        return fillableNodes;

    }

    private AreaNode findFirstEmpty(List<AreaNode> nodes)
    {
        int minDepth = nodes.get(0).getDepth();
        AreaNode firstEmpty = nodes.get(0);
        while(nodes.size()!=1)
        {
            if(nodes.get(1).getDepth()<firstEmpty.getDepth())
            {
                firstEmpty = nodes.get(1);
                minDepth = firstEmpty.getDepth();
                nodes.remove(0);
            }
            else
            {
                nodes.remove(1);
            }
        }
        return firstEmpty;
    }


    private List<AreaNode> readAreasFromFile(String path)
    {
        Path fileName = Path.of(path);
        String aree = null;
        try {
            aree = Files.readString(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String areaCode = null;
        String areaDescription = null;
        List<AreaNode> nodesFromFile = new LinkedList<AreaNode>();
        int posizionePartenza;
        int posizioneArrivo = 0;
        String areeMozzata = aree;
        while (areeMozzata.contains("code")) {
            posizionePartenza = areeMozzata.indexOf("code") + 8;

            if (areeMozzata.contains(",")) {
                posizioneArrivo = areeMozzata.indexOf(",") - 1;
                areaCode = areeMozzata.substring(posizionePartenza, posizioneArrivo);
            }

            areeMozzata = areeMozzata.substring(posizioneArrivo + 2);

            posizionePartenza = areeMozzata.indexOf("description") + 15;


            if (areeMozzata.contains(",")) {
                posizioneArrivo = areeMozzata.indexOf(",") - 3;
                areaDescription = areeMozzata.substring(posizionePartenza, posizioneArrivo - 1);
            }
            else
            {


                if (areeMozzata.contains("]"))
                {
                    posizioneArrivo = areeMozzata.indexOf("]") - 5;
                    areaDescription = areeMozzata.substring(posizionePartenza, posizioneArrivo - 1);
                }
            }

            if (areeMozzata.length() >= (posizioneArrivo + 2))
                areeMozzata = areeMozzata.substring(posizioneArrivo + 4);

            Area area = new Area(areaCode,areaDescription);
            AreaNode anode = new AreaNode(area);
            nodesFromFile.add(anode);
        }
        return nodesFromFile;
    }

    public List<AreaNode> getAreas(AreaNode nodoPartenza)
    {
        List<AreaNode> nodiDaRestituire = new LinkedList<>();

        if(childlessNode(nodoPartenza))
        {
            nodiDaRestituire.add(nodoPartenza);
            return nodiDaRestituire;
        }
        else
        {
            nodiDaRestituire.addAll(getAreas(nodoPartenza.getLeft()));
            nodiDaRestituire.addAll(getAreas(nodoPartenza.getRight()));
        }
        nodiDaRestituire.add(nodoPartenza);
        return nodiDaRestituire;

    }

    public List<AreaNode> getAreasOfCertainLevel(int level)
    {
        int i=0;
        List<AreaNode> areas = this.getAreas(root);
        while(i<areas.size())
        {
            if(areas.get(i).getDepth()==level)
            {
                i++;
            }
            else
            {
                areas.remove(i);
            }
        }
        return areas;
    }

    public AreaNode searchNodeByAreaID(AreaNode node,String code)
    {
        if(node.getArea().getCode().equals(code))
        {
            return node;
        }
        if (childlessNode(node))
            return null;
        AreaNode nodeFound = searchNodeByAreaID(node.getLeft(),code);
        if(nodeFound!=null)
        {
            return nodeFound;
        }
        nodeFound = searchNodeByAreaID(node.getRight(),code);
        if(nodeFound!=null)
        {
            return nodeFound;
        }

        return null;
    }

    public boolean childlessNode(AreaNode node)
    {
        if(node.isLeftNull() && node.isRightNull())
        {
            return true;
        }
        return false;
    }

}
