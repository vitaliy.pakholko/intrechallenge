public class Operatore
{
    private String username;

    public Operatore(String username)
    {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }

    public boolean equals(Operatore operatore)
    {
        return this.username.equals(operatore.getUsername());
    }

    public String toString()
    {
        return this.getUsername();
    }

}
