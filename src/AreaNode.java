import java.util.List;

public class AreaNode
{
    Area area;
    AreaNode left, right, parent;
    int depth;

    AreaNode(Area area){
        this.area = area;
        left = null;
        right = null;
    }

    public Area getArea() {
        return area;
    }

    public AreaNode getLeft() {
        return left;
    }

    public void setLeft(AreaNode left) {
        this.left = left;
    }

    public AreaNode getRight() {
        return right;
    }

    public void setRight(AreaNode right) {
        this.right = right;
    }

    public AreaNode getParent() {
        return parent;
    }

    public void setParent(AreaNode parent) {
        this.parent = parent;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public boolean isLeftNull()
    {
        if (this.left == null)
            return true;
        return false;
    }

    public boolean isRightNull()
    {
        if (this.right == null)
            return true;
        return false;
    }

    public String toString()
    {
        return "Area:\nCodice:" + this.area.getCode() + "\nDescrizione:" + this.area.getDescription();
    }
}