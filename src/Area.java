public class Area
{
    String code;
    String description;

    public Area(String code, String description)
    {
        this.code = code;
        this.description = description;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getCode()
    {
        return this.code;
    }

}
